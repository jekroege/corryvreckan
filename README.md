![](doc/logo_small.png)

# Corryvreckan
### The Maelstrom for Your Test Beam Data

This repo contains a bunch of WIP branches too specific to be merged but used for the analysis done for my thesis:

  * `rotation_analysis`: ATLASpix rotation scans with DESY data, used to extract the active depth from the angle dependence of the cluster column/row width
  * `investigate_spidr_signal_in_desy202007`: Analysis of the fast ADC input on the SPIDR board into which the TLU trigger was fed at DESY. Note: The TLU output was binned with 25ns, such that this analysis was not very useful. But the same code can be used at SPS, when the SPIDR ADC inputs are fed with the fast scintillators.
  * `analysis_dut_binning`: Binning of DUT analysis, TPX3 debugging histograms
  * `more_cluster_maps`: maps of associated clusters for different cluster sizes
  * `lab_analysis`: used for laboratory studies (no T0 signal required for the ATLASpix)

For more details about the project please have a look at the website at https://cern.ch/corryvreckan.

